import java.util.Stack;

public class State {

    private Stack<Character> chain = new Stack<Character>(); //цепочка символов
    private Stack<Character> stack = new Stack<Character>(); //магазинная память

    public State(char[] chain, char[] stack) {
        for (int i = 0; i < chain.length; i++) {
            this.chain.push(chain[chain.length - 1 - i]);
        }
        for (int i = 0; i < stack.length; i++) {
            this.stack.push(stack[stack.length - 1 - i]);
        }
    }

    public State(Stack<Character> chain, Stack<Character> stack) {
        this.chain = chain;
        this.stack = stack;
    }

    public void shiftTop() {

        /* Переместить верхушку стека */

        if (!chain.empty()) {
            stack.push(chain.pop());
        }
    }

    public boolean isFinished(char endSymbol) {

        /* Возвращает true, если цепочка символов полностью съедена, а в стеке только конечный нетерминал endSymbol */

        if((chain.empty())&&(stack.peek().equals(endSymbol))&&(stack.size()==1)){
            return true;
        }
        return false;
    }

    Stack<Character> getChain() {
        return chain;
    }

    Stack<Character> getStack() {
        return stack;
    }
}