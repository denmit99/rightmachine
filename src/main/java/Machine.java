import java.util.*;

public class Machine {

    private char endSymbol; //стартовый терминал
    private Grammar grammar; //грамматика, набор правил
    private final char[] terminals; //массив терминальных символов
    private final char[] nonTerminals; //массив нетерминальных символов

    public Machine(char endSymbol, Grammar grammar, char[] terminals, char[] nonTerminals) {
        this.endSymbol = endSymbol;
        this.grammar = grammar;
        this.terminals = terminals;
        this.nonTerminals = nonTerminals;
    }

    public boolean checkChain(String chain) {

        /*
         * Проверка на сооответствие цепочки символов автомату.
         * Все правила из грамматики, которые можно применить, применяются.
         * Новые состояния, получаеемые при переходе, добавляются в очередь transitions.
         * Как только найдена верная последовательность правил, распознающая цепочку, метод возвращает true.
         * Если очередь стала пустой, следовательно больше ни одно правило применить нельзя, метод вовзращает false.
         */

        LinkedList<State> transitions = new LinkedList<State>();
        transitions.add(new State(chain.toCharArray(), String.valueOf("").toCharArray()));

        while (true) {
            if (transitions.isEmpty()) {
                return false;
            }
            State nextState = transitions.peek();
            if (nextState.isFinished(endSymbol)) {
                return true;
            }
            addShiftState(nextState, transitions);
            addStates(nextState, transitions);
            transitions.poll();
           // printStack(transitions);
        }
    }


    public void addShiftState(State state, LinkedList<State> transitions) {

        /*
        * Добавить состояние, получаемое при переносе первого символа цепочки в магазинную память
        * */

        if (!state.getChain().empty()) {
            Stack<Character> chain = new Stack<Character>();
            Stack<Character> stack = new Stack<Character>();
            Stack<Character> originalStack = state.getStack();
            Stack<Character> originalChain = state.getChain();
            for (int i = 0; i < originalChain.size(); i++) {
                chain.add(originalChain.get(i));
            }
            for (int i = 0; i < originalStack.size(); i++) {
                stack.add(originalStack.get(i));
            }
            State newState = new State(chain, stack);
            newState.shiftTop();
            transitions.add(newState);
        }
    }

    public void addStates(State state, LinkedList<State> transitions) {

        /*
         * Применение всех возможных правил и добавление новых состояний в очередь
         * */

        Stack<Character> originalStack = state.getStack();
        Stack<Character> originalChain = state.getChain();
        String line = "";
        for (int j = 0; j < originalStack.size(); j++) {
            line = originalStack.get(originalStack.size() - 1 - j) + line;
            for (String s : grammar.getReversedTransitions(line)) {

                Stack<Character> chain = new Stack<Character>();
                Stack<Character> stack = new Stack<Character>();

                for (int i = 0; i < originalChain.size(); i++) {
                    chain.add(originalChain.get(i));
                }
                for (int i = 0; i < originalStack.size() - j - 1; i++) { //копируем все элементы кроме тех, которые поменяются после применения правила
                    stack.add(originalStack.get(i));
                }
                char[] symbols = s.toCharArray();
                for (int i = 0; i < symbols.length; i++) {
                    stack.push(symbols[symbols.length - 1 - i]);
                }
                transitions.add(new State(chain, stack));
            }
        }


    }

    public void printStack(LinkedList<State> transitions) {

        /*
         * Печатает стек текущих непроверенных состояний
         * */

        for (State s : transitions) {
            String chainLine = "", stackLine = "";
            for (int i = 0; i < s.getStack().size(); i++) {
                stackLine += s.getStack().get(i);
            }
            for (int i = 0; i < s.getChain().size(); i++) {
                chainLine += s.getChain().get(s.getChain().size() - 1 - i);
            }
            System.out.println("(" + chainLine + "," + stackLine + ")");
        }
        System.out.println("________");
    }
}