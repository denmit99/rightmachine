public class Rule {
    private String leftPart; //левая часть правила
    private String rightPart; //правая часть правила

    public Rule(String line) {

        if (!line.matches("[a-zA-Z]+->.+")) {
            System.out.println("This rule doesn't match the format. \"A->B\" required");
        } else {
            String[] symbols = line.split(" *-> *");
            leftPart = symbols[0];
            rightPart = symbols[1];
        }
    }

    public String getLeftPart() {
        return leftPart;
    }

    public String getRightPart() {
        return rightPart;
    }
}