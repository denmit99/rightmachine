import java.io.File;

/*
 * Программа строит правосторонний автомат по КСГ (файл grammar.txt)
 * и проверяет цепочку на соответствие автомату
 *
 * ******** ВХОДНЫЕ ПАРАМЕТРЫ ********
 * FILEPATH - Путь к файлу с грамматикой
 * CHAIN - Цепочка для проверки
 *
 * */

public class Main {

    public static final String FILEPATH = "src/main/resources/grammar.txt";
    public static final String CHAIN = "((a+b*c)+a)*(b+a)";

    public static void main(String[] args) {

        File grammarFile = new File(FILEPATH);
        FileParser fileParser = new FileParser();
        Grammar grammar = fileParser.parseFile(grammarFile);
        char[] nonTerminals = {'a', '+', '(', ')', '*'};
        char[] terminals = {'E', 'T', 'F'};
        Machine machine = new Machine('E', grammar, terminals, nonTerminals);
        if(machine.checkChain(CHAIN)){
            System.out.println("SUCCESS! Machine recognizes this chain!");
        } else {
            System.out.println("FAIL! Machine  doesn't recognizes this chain!");
        }
    }
}